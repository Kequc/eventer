class Eventer
{
    private _callbacks: { [index: string]: Function[] } = {};
    
    addListener (name: string, callback: Function)
    {
        if (!this._callbacks[name])
            this._callbacks[name] = [];
        
        if (this._callbacks[name].indexOf(callback) <= -1)
            this._callbacks[name].push(callback);
    }
    
    removeListener (name: string, callback: Function)
    {
        if (!this._callbacks[name])
            return;
        
        let index = this._callbacks[name].indexOf(callback);
        if (index > -1) {
            if (this._callbacks[name].length <= 1)
                delete this._callbacks[name];
            else
                this._callbacks[name].splice(index, 1);
        }
    }
    
    trigger (name: string, ...params: any[])
    {
        if (!this._callbacks[name])
            return;
        
        // run all callbacks
        let callbacks = this._callbacks[name].slice();

        for (let callback of callbacks) {
            callback(...params);
        }
    }
}

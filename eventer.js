var Eventer = (function () {
    function Eventer() {
        this._callbacks = {};
    }
    Eventer.prototype.addListener = function (name, callback) {
        if (!this._callbacks[name])
            this._callbacks[name] = [];
        if (this._callbacks[name].indexOf(callback) <= -1)
            this._callbacks[name].push(callback);
    };
    Eventer.prototype.removeListener = function (name, callback) {
        if (!this._callbacks[name])
            return;
        var index = this._callbacks[name].indexOf(callback);
        if (index > -1) {
            if (this._callbacks[name].length <= 1)
                delete this._callbacks[name];
            else
                this._callbacks[name].splice(index, 1);
        }
    };
    Eventer.prototype.trigger = function (name) {
        var params = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            params[_i - 1] = arguments[_i];
        }
        if (!this._callbacks[name])
            return;
        // run all callbacks
        var callbacks = this._callbacks[name].slice();
        for (var _a = 0, callbacks_1 = callbacks; _a < callbacks_1.length; _a++) {
            var callback = callbacks_1[_a];
            callback.apply(void 0, params);
        }
    };
    return Eventer;
}());
